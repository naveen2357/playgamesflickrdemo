//
//  FlickrCollectionViewCell.h
//  FlickrSearch
//
//  Created by Naveen Chaudhary on 08/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickrCollectionViewCell : UICollectionViewCell

-(void)setFlickrPhoto:(UIImage *)image;

@end
