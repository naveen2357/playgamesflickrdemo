//
//  ViewController.m
//  FlickrSearch
//
//  Created by Naveen Chaudhary on 08/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "ViewController.h"
#import "FlickrGalleryController.h"

static NSString *userDefaultsSearchTermsKey = @"searchTerms";

@interface ViewController ()<UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UISearchBar *searchBar;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:@"Search Flickr for images"];
    
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0.0,
                                                                   CGRectGetMaxY(self.navigationController.navigationBar.frame),
                                                                   self.view.bounds.size.width,
                                                                   44.0)];
    [self.searchBar setDelegate:self];
    [self.searchBar setPlaceholder:@"Search"];
    [self.searchBar setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [self.view addSubview:self.searchBar];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0,
                                                                   CGRectGetMaxY(self.searchBar.frame),
                                                                   self.view.bounds.size.width,
                                                                   self.view.bounds.size.height - CGRectGetMaxY(self.searchBar.frame))
                                                  style:UITableViewStylePlain];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    [self.tableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [self.view addSubview:self.tableView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    // adjust frames depending on device orientation
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        [self.searchBar setFrame:CGRectMake(0.0,
                                            CGRectGetMaxY(self.navigationController.navigationBar.frame),
                                            self.view.bounds.size.width,
                                            44.0)];
        [self.tableView setFrame:CGRectMake(0.0,
                                            CGRectGetMaxY(self.searchBar.frame),
                                            self.view.bounds.size.width,
                                            self.view.bounds.size.height - CGRectGetMaxY(self.searchBar.frame))];
    } completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        
    }];
}


#pragma mark - UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[NSUserDefaults standardUserDefaults] objectForKey:userDefaultsSearchTermsKey] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"SearchCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        
        UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0.0, cell.contentView.frame.size.height - 1.0, cell.contentView.frame.size.width, 1.0)];
        [separator setBackgroundColor:[UIColor colorWithWhite:0.95 alpha:1.0]];
        [separator setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
        [cell.contentView addSubview:separator];
    }
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    NSString *searchString = [[[NSUserDefaults standardUserDefaults] objectForKey:userDefaultsSearchTermsKey] objectAtIndex:indexPath.row];
    [cell.textLabel setText:searchString];
    return cell;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *searchString = [[[NSUserDefaults standardUserDefaults] objectForKey:userDefaultsSearchTermsKey] objectAtIndex:indexPath.row];
    NSMutableArray *arrSearchTerms = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:userDefaultsSearchTermsKey]];
    [arrSearchTerms removeObjectAtIndex:indexPath.row];
    [arrSearchTerms insertObject:searchString atIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithArray:arrSearchTerms] forKey:userDefaultsSearchTermsKey];
    
    [self showFlickrGalleryForSeachTerm:searchString];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UILabel *label = [[UILabel alloc] init];
    [label setText:@"No Recent Searches"];
    [label setTextAlignment:NSTextAlignmentCenter];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:userDefaultsSearchTermsKey] count] > 0) {
        [label setText:@""];
        return NULL;
    }
    return label;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:userDefaultsSearchTermsKey] count] == 0) {
        return 200.0;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *label = [[UILabel alloc] init];
    [label setText:@"Recent Searches"];
    [label setBackgroundColor:[UIColor colorWithWhite:0.95 alpha:1.0]];
    [label setTextAlignment:NSTextAlignmentCenter];
    return label;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:userDefaultsSearchTermsKey] count] == 0) {
        return 0;
    }
    return 44.0;
}

#pragma mark - UISearchBarDelegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSMutableArray *arrSearchTerms = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:userDefaultsSearchTermsKey]];
    [arrSearchTerms insertObject:searchBar.text atIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithArray:arrSearchTerms] forKey:userDefaultsSearchTermsKey];
    
    [self showFlickrGalleryForSeachTerm:searchBar.text];
}

-(void)showFlickrGalleryForSeachTerm:(NSString *)searchTerm {
    FlickrGalleryController *flickrVC = [[FlickrGalleryController alloc] init];
    [flickrVC setFlickrSearch:searchTerm];
    
    [self.navigationController pushViewController:flickrVC animated:YES];
    [self.tableView reloadData];
}

#pragma mark - keyboard notifications
-(void) keyboardDidShow:(NSNotification*) notification {
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect tableFrame = self.tableView.frame;
    tableFrame.size.height = self.view.bounds.size.height - CGRectGetMaxY(self.searchBar.frame) - keyboardFrame.size.height;
    [self.tableView setFrame:tableFrame];
}

-(void) keyboardWillHide:(NSNotification*) notification {
    CGRect tableFrame = self.tableView.frame;
    tableFrame.size.height = self.view.bounds.size.height - CGRectGetMaxY(self.searchBar.frame);
    
    double duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        [self.tableView setFrame:tableFrame];
    }];
}

@end
