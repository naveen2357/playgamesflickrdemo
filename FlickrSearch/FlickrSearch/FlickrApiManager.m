//
//  FlickrApiManager.m
//  FlickrSearch
//
//  Created by Naveen Chaudhary on 08/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "FlickrApiManager.h"

static FlickrApiManager *sharedManager = nil;
static NSString *key = @"f2ddfcba0e5f88c2568d96dcccd09602"; // flickr api key
static NSUInteger pageLimit = 20;   // fetch 20 images per api call

@interface FlickrApiManager ()
@property (nonatomic, strong) NSCache *imageCache;
@property (nonatomic) NSUInteger currentPageNumber;
@property (nonatomic, strong) NSString *currentSearchTerm;
@end

@implementation FlickrApiManager

+ (id)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[FlickrApiManager alloc] init];
    });
    return sharedManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.imageCache = [[NSCache alloc] init];
    }
    return self;
}

#pragma mark - flickr API
-(NSString *_Nullable)getImageUrlForPhoto:(NSDictionary *_Nullable)photoDict {
    return [NSString stringWithFormat:@"https://farm%@.static.flickr.com/%@/%@_%@.jpg", photoDict[@"farm"], photoDict[@"server"], photoDict[@"id"], photoDict[@"secret"]];
}

- (void)downloadImageWithUrl:(NSString *_Nullable)imageUrl
                withCallBack:(void (^ _Nullable)(UIImage * _Nullable))callBack {
    
    typeof(self) __weak weakSelf = self;
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:imageUrl]
            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    [weakSelf setCacheImage:image forKey:imageUrl];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        callBack(image);
                    });
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        callBack(NULL);
                    });
                }
            }] resume];
}

- (void)searchFlickrForText:(NSString *_Nullable)searchText
           exitingDataCount:(NSUInteger)dataCount
               withCallBack:(void (^_Nullable)(NSArray * _Nullable arrPhotos))callBack {
    
    searchText = [searchText stringByReplacingOccurrencesOfString:@" " withString:@"+"];    // replace spaces in search string with "+"
    NSUInteger pageNumber = dataCount/pageLimit + 1;
    
    if (self.currentSearchTerm == searchText && self.currentPageNumber == pageNumber) {
        callBack(NULL); // don't fetch again if same fetch already in progress
    } else {
        self.currentSearchTerm = searchText;
        self.currentPageNumber = pageNumber;
        
        NSString *urlString = [NSString stringWithFormat:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=%@&format=json&nojsoncallback=1&per_page=%lu&page=%lu&safe_search=1&text=%@", key, (unsigned long)pageLimit, (unsigned long)pageNumber, searchText];
        
        typeof(self) __weak weakSelf = self;
        NSURLSession *session = [NSURLSession sharedSession];
        [[session dataTaskWithURL:[NSURL URLWithString:urlString]
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if (data) {
                        NSDictionary *photosJSON = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                        NSArray *fetchedPhotos = photosJSON[@"photos"][@"photo"];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            callBack(fetchedPhotos);
                        });
                    } else {
                        // reset stored search parameters if call fails
                        [weakSelf resetSearchParameters];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            callBack(@[]);
                        });
                    }
                }] resume];
    }
}

-(void)resetSearchParameters {
    self.currentSearchTerm = NULL;
    self.currentPageNumber = 0;
}

#pragma mark - image cache
- (UIImage *_Nullable)getCachedImageForKey:(NSString *_Nullable)key {
    return [self.imageCache objectForKey:key];
}

- (void)setCacheImage:(UIImage*)image forKey:(NSString*)key {
    [self.imageCache setObject:image forKey:key];
}

@end
