//
//  FlickrPhotoLayout.m
//  FlickrSearch
//
//  Created by Naveen Chaudhary on 08/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "FlickrPhotoLayout.h"

@interface FlickrPhotoLayout ()

@property (nonatomic, strong) NSMutableDictionary *layoutInfo;
@property (nonatomic, strong) NSMutableArray *yOffsetArray;
@property (nonatomic) NSUInteger numberOfColumns;

@end

@implementation FlickrPhotoLayout

-(instancetype)init {
    self = [super init];
    if (self) {
        [self resetCachedLayout];
    }
    return self;
}

- (void)resetCachedLayout {
    self.yOffsetArray = [NSMutableArray array];
    self.layoutInfo = [NSMutableDictionary dictionary];
    self.numberOfColumns = 2;
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if (UIDeviceOrientationIsLandscape(orientation)) {
        self.numberOfColumns = 3;
    }
    
    for (NSUInteger i=0; i<self.numberOfColumns; i++) {
        [self.yOffsetArray addObject:@(0)];
    }
}

- (void)prepareLayout
{
    [self resetCachedLayout];
    
    NSIndexPath *indexPath;
    NSInteger itemCount = [self.collectionView numberOfItemsInSection:0];
    for (NSInteger item = 0; item < itemCount; item++) {
        indexPath = [NSIndexPath indexPathForItem:item inSection:0];
        UICollectionViewLayoutAttributes *itemAttributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
        itemAttributes.frame = [self frameForCellAtIndexPath:indexPath];
        self.layoutInfo[indexPath] = itemAttributes;
    }
}

- (CGSize)collectionViewContentSize
{
    CGFloat maxOffset = 0.0;
    for (NSNumber *yOffset in self.yOffsetArray) {
        if ([yOffset floatValue] > maxOffset) {
            maxOffset = [yOffset floatValue];
        }
    }
    return CGSizeMake(self.collectionView.bounds.size.width, maxOffset);
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *allAttributes = [NSMutableArray arrayWithCapacity:self.layoutInfo.count];
    [self.layoutInfo enumerateKeysAndObjectsUsingBlock:^(NSString *key, UICollectionViewLayoutAttributes *attributes, BOOL *stop) {
        if (CGRectIntersectsRect(rect, attributes.frame)) {
            [allAttributes addObject:attributes];
        }
    }];
    return allAttributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.layoutInfo[indexPath];
}

#pragma mark - custom methods
/* -frameForCellAtIndexPath: calculates the exact frame for each item */
- (CGRect)frameForCellAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat xPos = 0, yPos=0, width=0, height=0;
    
    NSUInteger columnNumber = indexPath.item % self.numberOfColumns;
    CGFloat minSpacing = 10.0;
    CGFloat xNegativeSpace = (self.numberOfColumns+1)*10.0;
    
    width = (self.collectionView.bounds.size.width-xNegativeSpace)/self.numberOfColumns;
    xPos = minSpacing + columnNumber*(width + minSpacing);
    height = [self.delegate calculateHeightForItemAtIndexPath:indexPath withWidth:width];
    yPos = [self.yOffsetArray[columnNumber] floatValue] + minSpacing;
    self.yOffsetArray[columnNumber] = @([self.yOffsetArray[columnNumber] floatValue] + height + minSpacing);
    return CGRectMake(xPos, yPos, width, height);
}

@end
