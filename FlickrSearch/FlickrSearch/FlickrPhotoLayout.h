//
//  FlickrPhotoLayout.h
//  FlickrSearch
//
//  Created by Naveen Chaudhary on 08/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FlickerPhotoLayoutProtocol <NSObject>
-(CGFloat)calculateHeightForItemAtIndexPath:(NSIndexPath *)indexPath withWidth:(CGFloat)width;
@end

@interface FlickrPhotoLayout : UICollectionViewLayout
@property (nonatomic, weak) id<FlickerPhotoLayoutProtocol> delegate;
@end

