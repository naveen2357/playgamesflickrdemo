//
//  FlickrGalleryController.m
//  FlickrSearch
//
//  Created by Naveen Chaudhary on 08/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "FlickrGalleryController.h"
#import "FlickrCollectionViewCell.h"
#import "FlickrApiManager.h"
#import "FlickrPhotoLayout.h"

@interface FlickrGalleryController ()<UICollectionViewDataSource, FlickerPhotoLayoutProtocol>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *arrPhotos;
@property (strong, nonatomic) NSString *searchString;

@end

@implementation FlickrGalleryController

-(instancetype)init {
    self = [super init];
    if (self) {
        self.arrPhotos = [NSMutableArray array];
        [[FlickrApiManager sharedInstance] resetSearchParameters];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:self.searchString];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self createSubviews];
    
    [self fetchMoreDataAndReload];
}

- (void)createSubviews {
    FlickrPhotoLayout *layout = [[FlickrPhotoLayout alloc] init];
    [layout setDelegate:self];
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds
                                             collectionViewLayout:layout];
    self.collectionView.dataSource = self;
    [self.collectionView setBackgroundColor:[UIColor clearColor]];
    [self.collectionView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    [self.view addSubview:self.collectionView];
    
    [self.collectionView registerClass:[FlickrCollectionViewCell class] forCellWithReuseIdentifier:@"FlickrCell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// invalidate collectionView layout on device orientation change
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        [self.collectionView.collectionViewLayout invalidateLayout];
    } completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        
    }];
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arrPhotos.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FlickrCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FlickrCell" forIndexPath:indexPath];
    [cell setFlickrPhoto:NULL];
    
    NSDictionary *photoDict = self.arrPhotos[indexPath.item];
    NSString *imageUrl = [[FlickrApiManager sharedInstance] getImageUrlForPhoto:photoDict];
    UIImage *cachedImage = [[FlickrApiManager sharedInstance] getCachedImageForKey:imageUrl];
    if (cachedImage) {
        [cell setFlickrPhoto:cachedImage];
    } else {
        typeof(self) __weak weakSelf = self;
        [[FlickrApiManager sharedInstance] downloadImageWithUrl:imageUrl withCallBack:^(UIImage * _Nullable image) {
            [weakSelf.collectionView reloadItemsAtIndexPaths:@[indexPath]];
        }];
    }
    
    if (indexPath.row == self.arrPhotos.count - 1) {
        [self fetchMoreDataAndReload];  // load more on reaching the end
    }
    
    return cell;
}

#pragma mark - FlickerPhotoLayoutProtocol
-(CGFloat)calculateHeightForItemAtIndexPath:(NSIndexPath *)indexPath withWidth:(CGFloat)width {
    CGFloat height = 100.0;
    
    NSDictionary *photoDict = self.arrPhotos[indexPath.item];
    NSString *imageUrl = [[FlickrApiManager sharedInstance] getImageUrlForPhoto:photoDict];
    UIImage *cachedImage = [[FlickrApiManager sharedInstance] getCachedImageForKey:imageUrl];
    if (cachedImage) {
        height = width*cachedImage.size.height/cachedImage.size.width;
    }
    
    return height;
}

#pragma mark - public methods
-(void)setFlickrSearch:(NSString *)string {
    self.searchString = string;
}

#pragma mark - private methods
-(void)fetchMoreDataAndReload {
    typeof(self) __weak weakSelf = self;
    [[FlickrApiManager sharedInstance] searchFlickrForText:self.searchString
                                          exitingDataCount:self.arrPhotos.count
                                              withCallBack:^(NSArray * _Nullable arrPhotos) {
                                                  [weakSelf.arrPhotos addObjectsFromArray:arrPhotos];
                                                  [weakSelf.collectionView reloadData];
                                              }];
}

@end
