//
//  FlickrApiManager.h
//  FlickrSearch
//
//  Created by Naveen Chaudhary on 08/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FlickrApiManager : NSObject

+ (FlickrApiManager *_Nullable)sharedInstance;

- (void)searchFlickrForText:(NSString *_Nullable)searchText exitingDataCount:(NSUInteger)dataCount withCallBack:(void (^_Nullable)(NSArray * _Nullable arrPhotos))callBack;
- (void)downloadImageWithUrl:(NSString *_Nullable)imageUrl withCallBack:(void (^_Nullable)(UIImage * _Nullable image))callBack;
- (UIImage*_Nullable)getCachedImageForKey:(NSString*_Nullable)key;
- (NSString *_Nullable)getImageUrlForPhoto:(NSDictionary *_Nullable)photoDict;
- (void)resetSearchParameters;

@end
