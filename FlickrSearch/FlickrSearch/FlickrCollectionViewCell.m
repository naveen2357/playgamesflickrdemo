//
//  FlickrCollectionViewCell.m
//  FlickrSearch
//
//  Created by Naveen Chaudhary on 08/06/17.
//  Copyright © 2017 Naveen Chaudhary. All rights reserved.
//

#import "FlickrCollectionViewCell.h"

@interface FlickrCollectionViewCell ()

@property (nonatomic, strong) UIImageView *flickrImageView;

@end

@implementation FlickrCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.flickrImageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        [self.flickrImageView setBackgroundColor:[UIColor colorWithWhite:0.95 alpha:1.0]];
        [self.flickrImageView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self.flickrImageView setContentMode:UIViewContentModeScaleAspectFit];
        [self.contentView addSubview:self.flickrImageView];
    }
    return self;
}

#pragma mark - public methods

-(void)setFlickrPhoto:(UIImage *)image {
    [self.flickrImageView setImage:image];
}

@end
